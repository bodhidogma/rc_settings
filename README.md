# rc_settings

Confgig / Tuning settings for various RC devices:

## Quads:
 * F450_Naze32.cfg - F450, SunnySky2212, RCMC 30A, Naze32

## RC Explorer Tricopters:
 * TricopterV25_Naze32.cfg - Tri V2.5, SunnySky2212 / 980KV, RTF Mini 30A (blk), Naze32v6
 * TricopterV4_F3FC.cfg - Tri V4, BE2217 / 1300KV, LittleBee 30A, F3FC
 * MiniTriV1_F3FC.cfg - MiniTri V1, EMAX RS2205 / 2300KV, LittleBee 30a, F3FC

